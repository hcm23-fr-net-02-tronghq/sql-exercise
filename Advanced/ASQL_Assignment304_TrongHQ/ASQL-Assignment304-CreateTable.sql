CREATE DATABASE DMS

USE [DMS]
GO
/****** Object:  Table [dbo].[EMPMAJOR]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPMAJOR](
	[emp_no] [char](6) NOT NULL,
	[major] [char](3) NOT NULL,
	[major_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[major] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMP]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMP](
	[emp_no] [char](6) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[dept_no] [char](3) NOT NULL,
	[job] [varchar](50) NULL,
	[salary] [money] NOT NULL,
	[bonus] [money] NULL,
	[ed_level] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DEPT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DEPT](
	[dept_no] [char](3) NOT NULL,
	[dept_name] [varchar](50) NOT NULL,
	[mgn_no] [char](6) NULL,
	[admr_dept] [char](3) NOT NULL,
	[location] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[dept_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[dept_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMPPROJACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPPROJACT](
	[emp_no] [char](6) NOT NULL,
	[proj_no] [char](6) NOT NULL,
	[act_no] [int] NOT NULL,
 CONSTRAINT [PK_EPA] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[proj_no] ASC,
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACT](
	[act_no] [int] NOT NULL,
	[act_des] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Dept]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[DEPT]  WITH CHECK ADD  CONSTRAINT [FK_Dept] FOREIGN KEY([mgn_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[DEPT] CHECK CONSTRAINT [FK_Dept]
GO
/****** Object:  ForeignKey [FK__EMP__dept_no__3E52440B]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMP]  WITH CHECK ADD FOREIGN KEY([dept_no])
REFERENCES [dbo].[DEPT] ([dept_no])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_Major]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPMAJOR]  WITH CHECK ADD  CONSTRAINT [FK_Major] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPMAJOR] CHECK CONSTRAINT [FK_Major]
GO
/****** Object:  ForeignKey [FK_EPA1]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA1] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA1]
GO
/****** Object:  ForeignKey [FK_EPA2]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA2] FOREIGN KEY([act_no])
REFERENCES [dbo].[ACT] ([act_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA2]
GO

-- Adding records to the DEPT table
INSERT INTO DEPT (dept_no, dept_name, mgn_no, admr_dept, location) VALUES
('A00', 'Human Resources', null, 'A00', 'New York'),
('B01', 'Marketing', null, 'A00', 'Los Angeles'),
('C01', 'Finance', null, 'A00', 'Chicago'),
('D01', 'Information Technology', null, 'A00', 'San Francisco'),
('E01', 'Sales', null, 'A00', 'Boston'),
('F01', 'Engineering', null, 'A00', 'Seattle'),
('G01', 'Customer Support', null, 'A00', 'Dallas'),
('H01', 'Administration', null, 'A00', 'Miami');

-- Adding records to the ACT table
INSERT INTO ACT (act_no, act_des) VALUES
(1, 'Activity 1'),
(2, 'Activity 2'),
(3, 'Activity 3'),
(4, 'Activity 4'),
(5, 'Activity 5'),
(6, 'Activity 6'),
(7, 'Activity 7'),
(8, 'Activity 8');

-- Adding records to the EMP table
INSERT INTO EMP (emp_no, last_name, first_name, dept_no, job, salary, bonus, ed_level) VALUES
('000010', 'Haas', 'Christine', 'A00', 'HR Specialist', 52750.00, NULL, 3),
('000020', 'Smith', 'Tom', 'B01', 'Marketing Manager', 72500.00, 2500.00, 4),
('000030', 'Johnson', 'Alice', 'C01', 'Financial Analyst', 63500.00, 1500.00, 3),
('000040', 'Williams', 'David', 'D01', 'IT Director', 89000.00, 5000.00, 5),
('000050', 'Brown', 'Eva', 'E01', 'Sales Representative', 57000.00, 2000.00, 2),
('000060', 'Lee', 'Michael', 'F01', 'Software Engineer', 72000.00, NULL, 4),
('000070', 'Turner', 'Sophia', 'G01', 'Support Specialist', 62000.00, 1000.00, 3),
('000080', 'Garcia', 'Luis', 'H01', 'Administrator', 60000.00, 2000.00, 4);
-- Update mgn_no in DEPT based on manager's employee number from EMP
UPDATE D
SET D.mgn_no = E.emp_no
FROM DEPT D
INNER JOIN EMP E ON D.dept_no = E.dept_no;


-- Adding records to the EMPMAJOR table
INSERT INTO EMPMAJOR (emp_no, major, major_name) VALUES
('000010', 'MAT', 'Mathematics'),
('000020', 'CSI', 'Computer Science'),
('000030', 'MAT', 'Mathematics'),
('000040', 'IT', 'Information Technology'),
('000050', 'CSI', 'Computer Science'),
('000060', 'IT', 'Information Technology'),
('000070', 'MAT', 'Mathematics'),
('000080', 'MAT', 'Mathematics');

-- Adding records to the EMPPROJACT table
INSERT INTO EMPPROJACT (emp_no, proj_no, act_no) VALUES
('000010', 'P001', 1),
('000020', 'P001', 2),
('000030', 'P002', 3),
('000040', 'P003', 4),
('000050', 'P003', 5),
('000060', 'P002', 6),
('000070', 'P001', 7),
('000080', 'P004', 8);
