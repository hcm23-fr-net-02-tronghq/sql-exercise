use QLNV
--2 Specify the names of the employees whose have skill of �Java�
--A) Join Selection

SELECT e.Employee_Name
FROM Employee_Table e 
	JOIN Employee_Skill_Table es ON e.Employee_Number = es.Employee_Number 
	JOIN Skill_Table s ON es.Skill_Code = s.Skill_Code
WHERE s.Skill_Name = 'Java'

--B) Sub query
SELECT e.Employee_Name
FROM Employee_Table e
WHERE Employee_Number IN (
	SELECT Employee_Number
	FROM Employee_Skill_Table es
	WHERE es.Skill_Code IN (
		SELECT Skill_Code
		FROM Skill_Table s
		WHERE s.Skill_Name = 'Java'
	)
)

--3 Specify the departments which have >=3 employees,
--print out the list of departments� employees right after each department.

SELECT e.Employee_Name, d.Department_Name
FROM Employee_Table e JOIN Department d ON d.Department_Number = e.Department_Number
WHERE e.Department_Number IN (
	SELECT Department_Number 
	FROM Employee_Table
	GROUP BY Department_Number
	HAVING COUNT(*) >= 3
)

--4.Use SUB-QUERY technique to list out the different employees 
--(include employee number and employee names) who have multiple skills.

SELECT e.Employee_Number, e.Employee_Name
FROM Employee_Table e
WHERE e.Employee_Number IN (
	SELECT es.Employee_Number
	FROM Employee_Skill_Table es
	GROUP BY Employee_Number
	HAVING COUNT(*) > 1
)

--5 Create a view to show different employees (with following information: 
--employee number and employee name, department name) who have multiple skills.

CREATE VIEW [Employee_Multiple_Skill] AS
SELECT e.Employee_Number, e.Employee_Name, d.Department_Number
FROM Employee_Table e JOIN Department d ON e.Department_Number = d.Department_Number
WHERE e.Employee_Number IN (
	SELECT es.Employee_Number
	FROM Employee_Skill_Table es
	GROUP BY Employee_Number
	HAVING COUNT(*) > 1
)