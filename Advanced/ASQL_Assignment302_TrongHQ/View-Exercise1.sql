--use QLBH
CREATE VIEW [QUAN_SAT_DON_HANG] AS
SELECT kh.Ma_KH, dh.Ngay_DH,sp.Ten_SP,dh.So_Luong, sp.Don_Gia * dh.So_Luong AS Thanh_Tien
FROM Don_Hang dh JOIN Khach_Hang kh ON dh.Ma_KH = kh.Ma_KH JOIN San_Pham sp ON dh.Ma_SP = sp.Ma_SP  