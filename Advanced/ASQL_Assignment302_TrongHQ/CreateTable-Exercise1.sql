--CREATE DATABASE QLBH

use QLBH
-- Create the San_Pham (Products) table
CREATE TABLE San_Pham (
    Ma_SP INT PRIMARY KEY,
    Ten_SP VARCHAR(255) NOT NULL,
    Don_Gia DECIMAL(10, 2) NOT NULL
);

-- Insert sample data into San_Pham
INSERT INTO San_Pham (Ma_SP, Ten_SP, Don_Gia)
VALUES
    (1, 'Product A', 10.99),
    (2, 'Product B', 15.49),
    (3, 'Product C', 8.95);

-- Create the Khach_Hang (Customers) table
CREATE TABLE Khach_Hang (
    Ma_KH INT PRIMARY KEY,
    Ten_KH VARCHAR(255) NOT NULL,
    Phone_No VARCHAR(15),
    Ghi_Chu TEXT
);

-- Insert sample data into Khach_Hang
INSERT INTO Khach_Hang (Ma_KH, Ten_KH, Phone_No, Ghi_Chu)
VALUES
    (1, 'Hoang Quoc Trong', '123-456-7890', 'Regular customer'),
    (2, 'Hoang Nhat Tien', '987-654-3210', 'VIP customer'),
    (3, 'Cao Nguyen Minh Quan', '555-555-5555', NULL);

-- Create the Don_Hang (Orders) table
CREATE TABLE Don_Hang (
    Ma_DH INT PRIMARY KEY,
    Ngay_DH DATE NOT NULL,
    Ma_SP INT,
    So_Luong INT NOT NULL,
    Ma_KH INT,
    FOREIGN KEY (Ma_SP) REFERENCES San_Pham(Ma_SP),
    FOREIGN KEY (Ma_KH) REFERENCES Khach_Hang(Ma_KH)
);

-- Insert sample data into Don_Hang
INSERT INTO Don_Hang (Ma_DH, Ngay_DH, Ma_SP, So_Luong, Ma_KH)
VALUES
    (1, '2023-09-28', 1, 2, 1),
    (2, '2023-09-26', 2, 3, 2),
    (3, '2023-09-24', 3, 1, 3);