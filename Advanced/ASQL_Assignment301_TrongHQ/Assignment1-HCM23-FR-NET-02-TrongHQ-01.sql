use EMS

-- Delete all data from the Emp_Skill table
DELETE FROM [dbo].[Emp_Skill];

-- Delete all data from the Employee table
DELETE FROM [dbo].[Employee];

-- Delete all data from the Skill table
DELETE FROM [dbo].[Skill];

-- Delete all data from the Department table
DELETE FROM [dbo].[Department];


-- Insert data into the Department table
INSERT INTO [dbo].[Department] ([DeptName], [Note])
VALUES
    (N'IT Department', NULL),
    (N'HR Department', NULL),
    (N'Sales Department', NULL),
    (N'Marketing Department', NULL),
    (N'Finance Department', NULL),
    (N'Operations Department', NULL),
    (N'Research and Development', NULL),
    (N'Customer Support', NULL);

-- Insert data into the Skill table
INSERT INTO [dbo].[Skill] ([SkillName], [Note])
VALUES
    (N'C++', NULL),
    (N'.NET', NULL),
    (N'JAVA', NULL),
    (N'REACT', NULL),
    (N'JAVA', NULL),
    (N'.NET', NULL),
    (N'REACT', NULL),
    (N'BA', NULL);


	-- Insert data into the Employee table
INSERT INTO [dbo].[Employee] ([EmpNo], [EmpName], [BirthDay], [DeptNo], [MgrNo], [StartDate], [Salary], [Status], [Note], [Level], [Email])
VALUES
     (1, N'John Doe', '1990-05-15', 1, NULL, '2015-07-01', 60000, 1, NULL, 5, N'john.doe@example.com'),
    (2, N'Jane Smith', '1985-12-20', 3, 1, '2018-03-10', 75000, 1, NULL, 6, N'jane.smith@example.com'),
    (3, N'Mark Johnson', '1993-08-05', 4, 1, '2020-02-15', 55000, 1, NULL, 4, N'mark.johnson@example.com'),
    (4, N'Susan Lee', '1988-04-03', 7, 2, '2017-09-22', 70000, 1, NULL, 5, N'susan.lee@example.com'),
    (5, N'Michael Brown', '1992-06-10', 6, NULL, '2019-11-05', 62000, 1, NULL, 4, N'michael.brown@example.com'),
    (6, N'Emily Davis', '1991-03-18', 1, 3, '2016-08-12', 68000, 1, NULL, 5, N'emily.davis@example.com'),
    (7, N'David Wilson', '1987-11-28', 3, 2, '2021-04-30', 80000, 1, NULL, 6, N'david.wilson@example.com'),
    (8, N'Amy Clark', '1986-09-07', 4, 3, '2017-12-15', 72000, 1, NULL, 5, N'amy.clark@example.com');


INSERT INTO [dbo].[Emp_Skill] ([SkillNo], [EmpNo], [SkillLevel], [RegDate], [Description])
VALUES
    (1, 1, 5, '2015-07-01', N'C++'),
    (2, 1, 4, '2015-07-01', N'.NET'),
    (3, 2, 5, '2018-03-10', N'JAVA'),
    (4, 2, 4, '2018-03-10', N'.NET'),
    (5, 3, 4, '2020-02-15', N'REACT'),
    (6, 4, 5, '2017-09-22', N'BA'),
    (7, 5, 4, '2019-11-05', N'QA'),
    (8, 6, 5, '2016-08-12', N'JAVA'),
    (1, 7, 6, '2021-04-30', N'.NET'),
    (2, 8, 5, '2017-12-15', N'C++');

