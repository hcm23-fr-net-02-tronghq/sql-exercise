
-- Problem 1
-- 1.Writes an UDF returns ProductID, Name, ProductNumber, Color and ListPrice
-- for products that have 'CA' in the ProductNumber and Color is 'black'

CREATE FUNCTION PRODUCT_SELECT()
RETURNS TABLE
AS
RETURN
(
	Select ProductID,Name,ProductNumber,Color,ListPrice
	from Production.Product
	WHERE ProductNumber LIKE 'CA%' AND Color = 'Black'
)


-- 2. Writes an inline table UDF that accept an integer parameter named 
-- BusinessEntityID and returns the associated address of a business entity.
SELECT * FROM PRODUCT_SELECT()

CREATE FUNCTION BUSINESS_ENTITY_ADDRESS (@BusinessEntityId INT)
RETURNS TABLE
AS
RETURN
(
	SELECT *
	FROM Person.BusinessEntityAddress
	WHERE BusinessEntityId = @BusinessEntityId
)

SELECT * FROM BUSINESS_ENTITY_ADDRESS(3)

-- Problem 2
ALTER PROCEDURE GET_PERSON
	@ModifiedDate Date, @UpperFlag Bit
AS
BEGIN
	SELECT
		CASE
			WHEN @UpperFlag = 1 THEN UPPER(p.FirstName)
			END AS FirstName
	FROM Person.Person p
	WHERE ModifiedDate > @ModifiedDate;
END;

exec GET_PERSON @ModifiedDate = '2008-01-01', @UpperFlag = 1

CREATE TABLE Production.ProductInventoryAudit (

       ProductID           INT NOT NULL,
       LocationID          SMALLINT NOT NULL,
       Shelf               NVARCHAR(10) NOT NULL,
       Bin                 TINYINT NOT NULL,
       Quantity            SMALLINT NOT NULL,
       rowguid             UNIQUEIDENTIFIER NOT NULL,
       ModifiedDate        DATETIME NOT NULL,
       InsertOrDelete      CHAR(1) NOT NULL
)


CREATE TRIGGER CAPTURE_RECORD
ON Production.ProductInventory
FOR INSERT, DELETE 
AS
IF EXISTS ( SELECT 1 FROM inserted)
BEGIN
	INSERT INTO Production.ProductInventoryAudit(ProductID, LocationID, Shelf, Bin, Quantity, rowguid, ModifiedDate, InsertOrDelete)
	SELECT i.ProductID,i.LocationID,i.Shelf,i.Bin,i.Quantity,i.rowguid,i.ModifiedDate, 'I'
	FROM inserted as i
END
ELSE
BEGIN
	INSERT INTO Production.ProductInventoryAudit(ProductID, LocationID, Shelf, Bin, Quantity, rowguid, ModifiedDate, InsertOrDelete)
	SELECT d.ProductID, d.LocationID, d.Shelf, d.Bin, d.Quantity, d.rowguid, d.ModifiedDate, 'D'
	FROM deleted AS d
END

DELETE From Production.ProductInventory where ProductID = 1
