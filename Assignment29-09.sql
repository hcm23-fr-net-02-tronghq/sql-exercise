﻿
CREATE DATABASE QLBH

use QLBH

CREATE TABLE KhachHang(
	MaKH INT PRIMARY KEY,
	HoTen nvarchar(255) NOT NULL,
	DiaChi nvarchar(255) NULL,
	SoDT nvarchar(10) NOT NULL,

	Doanhso INT NULL,
	NgDK Date NOT NULL
)

CREATE TABLE NhanVien(
	MaNV INT PRIMARY KEY,
	HoTen nvarchar(255) NOT NULL,
	SoDt nvarchar(10) NOT NULL,
	NgVaoLam Date NOT NULL,
)

CREATE TABLE SanPham(
	MaSP INT PRIMARY KEY,
	TenSP nvarchar(255) not null,
	DVT nvarchar (5) not null,
	NuocSanXuat nvarchar (255) not null,
	Gia money not null
)


CREATE TABLE HoaDon(
	SoHD INT PRIMARY KEY,
	NgHD Date not null,
	TriGia money not null,
	MaKH INT FOREIGN KEY REFERENCES KhachHang(MaKH),
	MaNV INT FOREIGN KEY REFERENCES NhanVien(MaNV)
)

CREATE TABLE CTHD(
	SL INT PRIMARY KEY,
	SoHD INT FOREIGN KEY REFERENCES HoaDon(SoHD),
	MaSP INT FOREIGN KEY REFERENCES SanPham(MaSP)
)

INSERT INTO KhachHang(MaKH, HoTen, DiaChi, SoDT, DoanhSo, NgDK)
VALUES
(1,'Hoang Quoc Trong', null, '12345678', null,'2001-11-11'),
(2,'Nguyen Van A', null, '12345678', 100000,'2001-11-11'),
(3,'Nguyen Van B', null, '12345678', 200000,'2001-11-11')

INSERT INTO NhanVien(MaNV, HoTen, SoDT, NgVaoLam)
VALUES
(1,'Hoang Quoc', '12345678','2001-11-11'),
(2,'Nguyen Van C', '12345678','2001-11-11'),
(3,'Nguyen Van D', '12345678','2001-11-11')

INSERT INTO SanPham(MaSP, TenSP, DVT, NuocSanXuat, Gia)
VALUES
(1,'San Pham A', 'VND', 'Viet Nam', 10000),
(2,'San Pham B', 'VND', 'Viet Nam', 20000),
(3,'San Pham C', 'VND', 'Viet Nam', 30000),
(4,'San Pham D', 'VND', 'Han Quoc', 30000)

INSERT INTO HoaDon(SoHD, NgHD, MaKH, MaNV, TriGia)
VALUES
(1,'2023-08-12', 2, 1, 50000),
(2,'2023-08-13', 2, 2, 50000),
(3,'2023-08-14', 3, 3, 200000),
(4,'2023-5-23',2,1,50000)


INSERT INTO CTHD(SoHD,MaSP,SL)
VALUES
(1,1,5),
(1,1,5),
(2,1,6),
(3,2,10),
(4,2,3)

INSERT INTO CTHD(SoHD,MaSP,SL)
VALUES
(1,3,11),
(1,2,18)
-- Câu 1: In ra danh sách các sản phẩm(MaSP, TenSP) do 
-- "Viet Nam" sản xuất hoặc các sản phẩm bán trong ngày 23/052023
SELECT distinct s.MaSP, s.TenSP
FROM SanPham s LEFT JOIN CTHD chd ON s.MaSP = chd.MaSP JOIN HoaDon hd ON hd.SoHD = chd.SoHD
WHERE NuocSanXuat = 'Viet Nam' OR hd.NgHD = '2023-5-23'


--- Câu 2: In ra danh sách các sản phẩm (MaSP, TenSP) không bán được
SELECT sp.MaSP, sp.TenSP
FROM SANPHAM sp left join CTHD chd ON sp.MaSP = chd.MaSP
WHERE chd.SoHD IS NULL

-- Câu 3: Tìm số hóa đon đã mua tất cả các sản phẩm do Viet Nam sản xuất

SELECT distinct chd.SoHD
FROM CTHD chd
WHERE chd.MaSP IN (
	SELECT MaSP
	FROM SanPham
	WHERE NuocSanXuat = 'Viet Nam'
) 

